﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTable
{
    public partial class MainForm : Form
    {
        private readonly HashTable<PassportData, Person> _table;

        public MainForm()
        {
            InitializeComponent();
            _table = new HashTable<PassportData, Person>();
        }

        private void AddToTable(Person person, int index = -1)
        {
            _table.Add(person.PassportData, person);
            if (index < 0)
                tableView.Rows.Add(person.PassportData.Series,
                    person.PassportData.Number,
                    person.Fullname,
                    person.Address);
            else
                tableView.Rows.Insert(index, person.PassportData.Series,
                    person.PassportData.Number,
                    person.Fullname,
                    person.Address);
        }

        private void OpenButtonClicked(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                _table.Clear();
                tableView.Rows.Clear();
                try
                {
                    using var reader = new PersonReader(new StreamReader(File.Open(openDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read)));
                    IEnumerable<Person> persons = reader.ReadPersons();
                    foreach (Person person in persons)
                        AddToTable(person);
                }
                catch
                {
                    _table.Clear();
                    tableView.Rows.Clear();
                    MessageBox.Show(this, $"Не удалось считать файл \"{ openDialog.FileName }\".", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void AddButtonClicked(object sender, EventArgs e)
        {
            var newPerson = new Person();
            using Form dialog = new DataAdd(newPerson);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    AddToTable(newPerson);
                }
                catch
                {
                    MessageBox.Show(this, "Не удалось добавить данные о человеке.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private Person CreatePersonFromTable(int rowIndex)
        {
            string series = (string)tableView.Rows[rowIndex].Cells[0].Value;
            string number = (string)tableView.Rows[rowIndex].Cells[1].Value;
            string fullname = (string)tableView.Rows[rowIndex].Cells[2].Value;
            string address = (string)tableView.Rows[rowIndex].Cells[3].Value;
            return new Person(new PassportData(series, number), fullname, address);
        }

        private void ModifyButtonClicked(object sender, EventArgs e)
        {
            int index = tableView.SelectedCells[0].RowIndex;
            var newPersonSuccess = CreatePersonFromTable(index);
            var newPersonAbort = CreatePersonFromTable(index);

            using Form dialog = new DataAdd(newPersonSuccess);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    RemoveAtIndex(index);
                    AddToTable(newPersonSuccess, index);
                }
                catch
                {
                    AddToTable(newPersonAbort, index);
                    MessageBox.Show(this, "Не удалось изменить данные о человеке.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void RemoveAtIndex(int index)
        {
            string series = (string)tableView.Rows[index].Cells[0].Value;
            string number = (string)tableView.Rows[index].Cells[1].Value;
            _table.Remove(new PassportData(series, number));
            tableView.Rows.RemoveAt(index);
        }

        private void RemoveButtonClicked(object sender, EventArgs e)
            => RemoveAtIndex(tableView.SelectedCells[0].RowIndex);

        private void FindButtonClicked(object sender, EventArgs e)
        {
            string series = seriesFindTextBox.Text;
            string number = numberFindTextBox.Text;
            var key = new PassportData(series, number);
            bool contains = _table.ContainsKey(key);
            MessageBox.Show(this,
                contains ? $"{ _table[key].Fullname }, { _table[key].Address }." : "Запись отсутствует.",
                contains ? "Найдено" : "Ошибка",
                MessageBoxButtons.OK,
                contains ? MessageBoxIcon.Information : MessageBoxIcon.Error);
        }

        private void SaveButtonClicked(object sender, EventArgs e)
        {
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using var writer = new PersonWriter(new StreamWriter(File.Open(saveDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.Read)));
                    var list = new List<Person>();
                    for (var i = 0; i < tableView.RowCount; ++i)
                    {
                        Person person = CreatePersonFromTable(i);
                        list.Add(person);
                    }
                    writer.WritePersons(list);
                }
                catch
                {
                    MessageBox.Show(this, $"Не удалось сохранить файл \"{ saveDialog.FileName }\".", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            modifyButton.Enabled = true;
            removeButton.Enabled = true;
        }

        private void RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (tableView.Rows.Count < 1)
            {
                modifyButton.Enabled = false;
                removeButton.Enabled = false;
            }
        }
    }
}
