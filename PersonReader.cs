﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HashTable
{
    public sealed class PersonReader : IDisposable
    {
        private const string Separator = ";";
        private readonly StreamReader _reader;

        public PersonReader(StreamReader reader)
            => _reader = reader;

        public void Dispose()
            => _reader?.Dispose();

        private static bool TryParse(string str, out Person person)
        {
            person = default;
            if (!string.IsNullOrEmpty(str))
            {
                string[] lines = str.Split(Separator, StringSplitOptions.TrimEntries);
                try
                {
                    person = new Person(new PassportData(lines[0], lines[1]), lines[2], lines[3]);
                    return true;
                }
                catch
                {

                }
            }
            return false;
        }

        public IEnumerable<Person> ReadPersons()
        {
            string line = _reader.ReadLine();
            while (TryParse(line, out Person person))
            {
                line = _reader.ReadLine();
                yield return person;
            }
        }
         
    }
}
