﻿using System;
using System.Windows.Forms;

namespace HashTable
{
    public partial class DataAdd : Form
    {
        private readonly Person _person;

        public DataAdd(Person person)
        {
            InitializeComponent();

            _person = person;

            seriesTextBox.DataBindings.Add(new Binding("Text", _person.PassportData, "Series"));
            numberTextBox.DataBindings.Add(new Binding("Text", _person.PassportData, "Number"));
            fullnameTextBox.DataBindings.Add(new Binding("Text", _person, "Fullname"));
            addressTextBox.DataBindings.Add(new Binding("Text", _person, "Address"));

            DialogResult = DialogResult.Cancel;
        }

        private void SaveClicked(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
