using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTable
{
    public class HashTable<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        private struct Index
        {
            private readonly HashTable<TKey, TValue> _table;
            private int _k;
            private int _index;

            public Index(HashTable<TKey, TValue> table, int index)
            {
                _table = table;
                _index = index;
                _k = 1;
            }

            public static Index operator++(Index index)
            {
                index._index = (index._index + index._k) % index._table.Capacity;
                index._k *= 2;
                return index;
            }

            public static implicit operator int(Index index)
                => index._index;
        }

        private const double FullnessMax = 0.80;
        private KeyValuePair<TKey, TValue>?[] _arr;
        private readonly IEqualityComparer<TKey> _keyComparer;

        public int Count { get; private set; }

        public int Capacity
            => _arr.Length;

        public double Fullness
            => (double)Count / Capacity;

        public TValue this[TKey key] 
        { 
            get 
            {
                if (key is null)
                    throw new ArgumentNullException(nameof(key));

                Index index = GetRealPlace(key);
                if (_arr[index].HasValue)
                    return _arr[index].Value.Value;
                else
                    throw new KeyNotFoundException("Ключ не содержится в коллекции.");
            }
            set => Add(key, value);
        }

        public HashTable()
            : this(comparer: null)
        {
            
        }

        public HashTable(IEqualityComparer<TKey> comparer = null)
            : this (1, comparer)
        {
            
        }

        public HashTable(int capacity, IEqualityComparer<TKey> comparer = null)
        {
            if (capacity < 1)
                throw new ArgumentOutOfRangeException(nameof(capacity), "Значение должно быть больше 0.");
            
            _keyComparer = comparer ?? EqualityComparer<TKey>.Default;
            _arr = new KeyValuePair<TKey, TValue>?[capacity];
        }

        private void Resize()
        {
            var newHashTable = new HashTable<TKey, TValue>(Capacity * 2, _keyComparer);
            foreach (KeyValuePair<TKey, TValue> pair in this)
                newHashTable.Add(pair.Key, pair.Value);
            
            _arr = newHashTable._arr;
            Count = newHashTable.Count;
        }

        private Index GetSupposedPlace(TKey key)
            => new(this, Math.Abs(_keyComparer.GetHashCode(key)) % Capacity);

        private Index GetRealPlace(TKey key)
        {
            if (Fullness >= FullnessMax)
                Resize();

            Index index = GetSupposedPlace(key);
            while (true)
            {
                if (!_arr[index].HasValue || _keyComparer.Equals(_arr[index].Value.Key, key))
                    return index;
                ++index;
            }
        }

        private int GetDistance(int firstIndex, int secondIndex)
            => (secondIndex + Capacity - firstIndex) % Capacity;

        private static void Swap<T>(ref T first, ref T second)
        {
            T temp = first;
            first = second;
            second = temp;
        }

        public void Add(TKey key, TValue value)
        {
            if (key is null)
                throw new ArgumentNullException(nameof(key));

            int index = GetRealPlace(key);
            if (_arr[index].HasValue)
                throw new ArgumentException("Элемент с таким ключом уже существует.");

            _arr[index] = new KeyValuePair<TKey, TValue>(key, value);
            ++Count;
        }

        public void Clear()
        {
            Count = 0;
            _arr = new KeyValuePair<TKey, TValue>?[_arr.Length];
        }
        
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex = 0)
        {
            if (array is null)
                throw new ArgumentNullException(nameof(array));

            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException(nameof(arrayIndex), "Значение должно быть больше 0.");
            
            if (arrayIndex + Count > array.Length)
                throw new ArgumentException("Число элементов в исходной коллекции больше доступного места от положения, заданного значением параметра index, до конца массива назначения array.");

            foreach (KeyValuePair<TKey, TValue> pair in this)
                array[arrayIndex++] = pair;
        }

        public bool Remove(TKey key)
        {
            if (key is null)
                return false;
            
            Index window = GetRealPlace(key);
            if (!_arr[window].HasValue)
                return false;

            _arr[window] = null;
            Index next = window;
            ++next;
            while (_arr[next].HasValue)
            {
                if (GetDistance(GetSupposedPlace(_arr[next].Value.Key), next) >= GetDistance(window, next))
                {
                    Swap(ref _arr[next], ref _arr[window]);
                    window = next;
                }
                ++next;
            }
            
            --Count;
            return true;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
            => _arr
                .Where(pair => pair is not null)
                .Select(pair => pair.Value)
                .GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();

        public bool ContainsKey(TKey key)
            => _arr[GetRealPlace(key)].HasValue;

        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default;

            if (key is null)
                return false;

            Index index = GetRealPlace(key);
            if (_arr[index].HasValue)
            {
                value = _arr[index].Value.Value;
                return true;
            }
            
            return false;
        }

        public override string ToString()
        {
            const string DictionaryPattern = "{{\"{0}\", \"{1}\"}}";
            var builder = new StringBuilder("[");
            var pairs = ((IEnumerable<KeyValuePair<TKey, TValue>>)this).ToArray();
            if (pairs.Length > 0)
            {
                builder.AppendFormat(DictionaryPattern, pairs[0].Key, pairs[0].Value);
                for (int i = 1; i < pairs.Length; ++i)
                    builder.Append(", ").AppendFormat(DictionaryPattern, pairs[i].Key, pairs[i].Value);
            }
            builder.Append(']');
            return builder.ToString();
        }
    }
}