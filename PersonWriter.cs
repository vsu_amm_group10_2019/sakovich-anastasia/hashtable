﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HashTable
{
    public sealed class PersonWriter : IDisposable
    {
        private const string Separator = ";";
        private readonly StreamWriter _writer;

        public PersonWriter(StreamWriter writer)
            => _writer = writer;

        private static string PersonToString(Person person)
            => string.Join(Separator, person.PassportData.Series,
                person.PassportData.Number,
                person.Fullname,
                person.Address);

        public void Dispose()
            => _writer.Dispose();

        public void WritePersons(IEnumerable<Person> persons)
        {
            using IEnumerator<Person> enumerator = persons.GetEnumerator();
            if (enumerator.MoveNext())
            {
                string line = PersonToString(enumerator.Current);
                _writer.Write(line);
                while (enumerator.MoveNext())
                {
                    line = PersonToString(enumerator.Current);
                    _writer.WriteLine();
                    _writer.Write(line);
                }
            }
        }
    }
}
