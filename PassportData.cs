﻿using System;

namespace HashTable
{
    public class PassportData : IEquatable<PassportData>
    {
        public string Series { get; set; }
        public string Number { get; set; }

        public PassportData()
        {
            Series = string.Empty;
            Number = string.Empty;
        }

        public PassportData(string series, string number)
        {
            Series = series;
            Number = number;
        }

        public bool Equals(PassportData other)
            => Series == other.Series && Number == other.Number;

        public override int GetHashCode()
            => (Series, Number).GetHashCode();

        public override bool Equals(object obj)
            => Equals(obj as PassportData);
    }

}
