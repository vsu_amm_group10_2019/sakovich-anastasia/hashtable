﻿namespace HashTable
{
    public class Person
    {
        public PassportData PassportData { get; set; }
        public string Fullname { get; set; }
        public string Address { get; set; }

        public Person()
        {
            PassportData = new PassportData();
            Fullname = string.Empty;
            Address = string.Empty;
        }

        public Person(PassportData passportData, string fullname, string address)
        {
            PassportData = passportData;
            Fullname = fullname;
            Address = address;
        }
    }

}
